class Post < ApplicationRecord
  belongs_to :category
  belongs_to :user
  has_many :comments
  has_one_attached :image_file
  has_rich_text :body

  validates :title, presence: true
  validates :category_id, presence:true
  validates :body, presence: true

  def self.search(query)
    where("title like ? OR body like?", "%#{query}%", "%#{query}%")
  end
end
