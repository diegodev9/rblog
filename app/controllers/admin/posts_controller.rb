class Admin::PostsController < Admin::ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :verify_logged_in

  def new
    @page_title = 'Add Posts'
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      flash[:notice] = 'Post Created'
      redirect_to admin_posts_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @post.update(post_params)
      flash[:notice] = 'Post Updated'
      redirect_to admin_posts_path
    else
      render :new
    end
  end

  def destroy
    @post.destroy
    flash[:notice] = 'Post Removed'
    redirect_to admin_posts_path
  end

  def index
    if params[:search]
      @posts = Post.search(params[:search]).all.order('created_at DESC').paginate(per_page: 10, page: params[:page])
    else
      @posts = Post.all.order('created_at DESC').paginate(per_page: 10, page: params[:page])
    end
  end

  private

  def set_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :user_id, :category_id, :tags, :image, :body, :image_file)
  end
end
