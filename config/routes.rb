Rails.application.routes.draw do

  root 'posts#index'

  get 'contact', to: 'contacts#new'
  get 'about', to: 'about#index'
  get 'comments/new'
  get 'categories/show'

  resources 'posts', only: [:index, :show]
  resources 'contacts', only: [:new, :create]
  resources 'categories'

  namespace :admin do
    resources :categories, :users, :posts, :comments
    resources :sessions, only: [:new, :create]
    get 'login', to: 'sessions#new', as: 'login'
    get 'logout', to: 'sessions#destroy', as: 'logout'
    get 'comments/destroy'
  end

  resources :posts do
    resources :comments
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
